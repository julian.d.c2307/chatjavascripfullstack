const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const path = require('path');

const mongoose = require('mongoose');

const app = express();
const server = http.createServer(app);
const io = socketio.listen(server);

//Db connection
mongoose.connect('mongodb://localhost/chat-database')
 .then(db => console.log('db is connected'))
 .catch(err => console.log(err));

//settings
app.set('port', process.env.PORT || 3000);

require('./sockets')(io);

//enviar archivos estaticos
app.use(express.static(path.join(__dirname,'public')));

//ejecutar servidor
server.listen(app.get('port'), ()=> {
  console.log('server on port',app.get('port'));
});
